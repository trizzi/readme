### Quarterly Priorities
This content is meant to communicate how I intend to allocate my time. I review it weekly, but it should largely remain consistent on the time-scales of quarters.

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Sensing Mechanisms | Internal context gathering, customer interviews, analyst inquiries, competitive review | 30% |
| Core Team Engagement | UX, Development, Quality, Infrastructure shared Performance Indicators and Retrospectives | 20% |
| GTM Development | Use Case development, Opportunity Review, Sales Support, PMM/TMM Alignment | 15% |
| Product Experience | Think Big-Think Small, Walk-throughs, learning goals, direction content review | 15% |
| Product Performance Indicators | Performance Indicator instrumentation, understanding, goal setting and attainment | 10% |
| Personal Growth / Leadership Opportunities | Representing GitLab externally, Representing Product internally | 10% |

### Weekly Priorities
This content is meant to communicate my priorities on a weekly basis

#### Legend
These designations are added to the previous week's priority list when adding the current week's priority.

- **Y** - Completed
- **N** - Not completed

### Week of 8.7

This week should be a repeat of last week as I continue on planning and research.

- GCP Alliances conversation and follow-up
- Customer conversations and prep
- Milestone planning
- FY24 Q3 OKRs
- Release posts
- Direction updates
- Performance indicators


### Week of 7.31

- GCP Alliances conversation and follow-up
- Customer conversations and prep
- Milestone planning
- FY24 Q3 OKRs
- Release posts
- Direction updates
- Performance indicators

#### Retrospective

I finished a first draft of the OKRs and kicked off a research project to understand how people use GCP and GitLab. 

